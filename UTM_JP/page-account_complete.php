<?php
/*
template Name: account complete
*/
?>
<?php get_header(); ?>


<main class="g_main cf" role="main">

<section class="s_title cf">
	<div class="header">
		<a href="<?php echo home_url(); ?>">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/terra_utm_white.png">
	 </a>
	</div>

	<div class="title_wrap mt8 cf">
		<h1 class="title">Terraアカウントお申し込み</h1>
		<p class="desc01">ご登録頂くと、Terra Droneが提供する全てのサービスをご利用いただけます。</p>
		<p class="desc02">・Terra UTM (飛行回数２回まで無料)
		<span>・Terra Mapperクラウド版 (総計の処理２回まで無料)<span></p>
	</div>
</section>

<section class="s_content">
	<div class="inner">
			<div class="page_complete cf">
					<div class="title_wrap cf">
						<h1 class="title">Terraアカウントのお申し込みを<br>受付けました。
						</h1>
						<div class="txt_wrap cf">
							<p class="txt">
ご記入頂いた情報は送信されました。<br>
確認のため、お客様へ自動返信メールをお送りしました。<br>
お問い合わせ頂き、ありがとうございました。<br>
							</p>
						</div>
					</div>
					<div class="top_btn">
						<a href="<?php echo home_url(); ?>" class="op">トップに戻る</a>
					</div>
				</div>
	</div>
</section>

</main><!-- /g_main -->


<?php get_footer(); ?>
