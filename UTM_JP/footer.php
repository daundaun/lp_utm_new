
<footer class="g_footer cf">

	<div class="page_top cf">
		<a href="#page" id="scroll_top" class="scrolltop">
	　　　　<i class="fas fa-chevron-up icon"></i>
		</a>
	</div>

	<div class="g_footer_secondary cf">
	<div class="inner cf">
			<nav class="group_company cf">
			<ul class="list on cf">
			<p class="name">グループ会社</p>
				<li>
					<a href="http://terra-motors.com/jp/" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-terra_motors.png" alt="テラモーターズ株式会社">
					</a>
				</li>
				<li>
					<a href="http://unifly.aero/" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-unifly.png" alt="Unifly NV">
					</a>
				</li>
			</ul>
			<div class="g_footer_company cf">
				<p class="name">
					テラドローン株式会社
				</p>
				<div class="item first cf">
					Address:&nbsp;〒150-0001 東京都渋谷区神宮前5丁目53-67 コスモス青山SOUTH棟3F
				</div>
				<div class="item cf">
					<span class="first">Company TEL:&nbsp;03-6419-7193</span>
					<span class="secondary">Mail:&nbsp;<span class="rtl">moc.reppam-arret@troppus</span></span>
				</div>
				<div class="item cf">
					TerraMapper Support TEL:&nbsp;03-4405-4237
				</div>
			</div>
		</nav>
		<ul class="sns on cf">
				<li>
					<a href="https://www.facebook.com/TerraDrone.jp/" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-facebook_1.svg" alt="facebook">
					</a>
				</li>
				<li>
					<a href="https://twitter.com/Terra_Drone_" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-twitter_2.svg" alt="twitter">
					</a>
				</li>
				<li>
					<a href="https://jp.linkedin.com/company/terra-drone?trk=extra_biz_viewers_viewed" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon-linkedin_3.svg" alt="linkedin">
					</a>
				</li>
			</ul>
		<!-- /#sns nav -->
	</div>
</div>
<div class="g_footer_last cf">
	<div class="inner cf">
		<p class="logo">
			<a href="<?php echo home_url(); ?>" class="op">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/terra_utm_logo.png" alt="テラドローン株式会社">
			</a>
		</p>
		<p class="copy">© <?php echo date("Y"); ?> Terra Drone Corp.</p>
	</div>
</div>
</footer>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js"></script>
<script>
$(document).ready(function(){
  $('.imgslider-wrap').slick({
		autoplay: true,
		autoplaySpeed: 4000,
		dots: true,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		//pauseOnHover: false
  });
	$('#slider_wrap').slick({
		//arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		dots: true,
		fade: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		//pauseOnHover: false
	});
});

	$(function() {
    var topBtn = $('#scroll_top');

    topBtn.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 'slow');
        return false;
    });

		var header = $('header');
		$(window).scroll(function () {
				if ($(this).scrollTop() > 100) {
			header.css('border-bottom','1px solid #ececec');
				} else {
			header.css('border-bottom','0');
				}
		});
});

</script>

<?php wp_footer(); ?>
</body>
</html>
