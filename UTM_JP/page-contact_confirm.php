<?php
/*
template Name: contact confirm
*/
?>
<?php get_header(); ?>

<header>
<nav>
<div class="container cf">
<div class="nav_left">
	<a href="<?php echo home_url(); ?>">
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/terra_utm_logo.png">
 </a>
</div>
<ul class="nav_right">
<li><a href="#" class="white">ログイン</a></li>
<li><a href="<?php echo home_url(); ?>/contact" class="black">お問い合わせ</a></li>
</ul>
</div>
</nav>
</header>

<main class="g_main cf contact" role="main">

<section class="s_title cf">

	<div class="title_wrap mt8 cf">
		<h1 class="title">お問い合わせ</h1>
		<div class="contact_desc">
			<p class="desc03">Terra UTMに関してのお申し込み・お問い合わせは、<br>
お電話からでもご利用いただけます。</p>
			<p class="tel">03-4405-4237</p>
		</div>
		</div>
</section>

<section class="s_content">
	<div class="inner">
			 <p class="confirm_desc">下記の内容をご確認ください。</p>
		<?php if(have_posts()) : while(have_posts()) :the_post(); ?>
		<div class="g_form cf">
			<?php the_content(); ?>
		</div>
		<?php endwhile; endif; ?>
	</div>
</section>

</main><!-- /g_main -->


<?php get_footer(); ?>
