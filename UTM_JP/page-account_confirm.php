<?php
/*
template Name: account confirm
*/
?>
<?php get_header(); ?>


<main class="g_main cf" role="main">

<section class="s_title cf">
	<div class="header">
		<a href="<?php echo home_url(); ?>">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/img/terra_utm_white.png">
	 </a>
	</div>

	<div class="title_wrap mt8 cf">
		<h1 class="title">Terraアカウントお申し込み</h1>
		<p class="desc01">ご登録頂くと、Terra Droneが提供する全てのサービスをご利用いただけます。</p>
		<p class="desc02">・Terra UTM (飛行回数２回まで無料)
		<span>・Terra Mapperクラウド版 (総計の処理２回まで無料)<span></p>
	</div>
</section>

<section class="s_content">
	<div class="inner">
		<p class="confirm_desc">下記の内容をご確認ください。</p>
		<?php if(have_posts()) : while(have_posts()) :the_post(); ?>
		<div class="g_form cf">
			<?php the_content(); ?>
		</div>
		<?php endwhile; endif; ?>
	</div>
</section>

</main><!-- /g_main -->


<?php get_footer(); ?>
