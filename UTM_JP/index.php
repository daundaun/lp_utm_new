<?php get_header(); ?>

<header>
<nav>
<div class="container cf">
<div class="nav_left">
	<a href="<?php echo home_url(); ?>">
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/terra_utm_logo.png">
 </a>
</div>
<ul class="nav_right">
<li><a href="#" class="white">ログイン</a></li>
<li><a href="<?php echo home_url(); ?>/contact" class="black">お問い合わせ</a></li>
</ul>
</div>
</nav>
</header>


<span id="top"></span>
<main class="g_main cf" role="main">

<section class="s_video_wrap">
<video src="<?php echo get_template_directory_uri(); ?>/assets/img/comp3.mp4" autoplay loop muted poster="<?php echo get_template_directory_uri(); ?>/assets/img/comp.jpg">
</video>
<div class="app_wrap">
<div class="container cf">
 <div class="app_left">
	 <img src="<?php echo get_template_directory_uri(); ?>/assets/img/app.png">
 <div class="app_text">
	 <div class="app_text_title">
		 Terra UTM アプリ（iPad）
	 </div>
	 <div class="app_text_desc">
		 ドローン操縦者向けアプリを使うと、機体操作や自動航行、飛行可能エリアの確認や
飛行申請、天候の確認など、ドローンを飛行させるために必要な情報を現場で入手できます。
	 </div>
 </div><!-- /app_text -->
 </div>
 <a class="app_btn" href="#">
   <div class="img"></div>
	 <div class="app_btn_text">
		 Download on the <span class="white">App Store</span>
	 </div>
 </a><!-- /app_btn -->
</div>
</div>
</section><!-- /video_wrap -->

<section class="s_intro">
	<p class="text_title">一元管理することでドローンでの業務を効率化</p>
		<p class="text_desc1">現場ごとに飛行経路を作成、一元管理することでドローンでの業務を効率化します</p>
			<p class="text_desc2">Terra UTMとは3次元空間を飛び交うドローンに対して「空の道」を授けるシステムであり、<br>
今後の目視外飛行において必須のプラットフォームとなります。<br>
物流・災害救援・点検・土木測量・農業等様々な産業において、より安全にドローンの飛行を実現し、業務利用を促進します。</p>
</section>

<section class="s_droneuav cf">
	<div class="container cf">
	<div class="uav_left">
		<div class="text_wrap">
			<p class="text_title">ドローンUAVの運行管理システム</p>
			<p class="text_desc">管理画面からドローンの操作をより安全・簡単に操作出来ます。<br>
特に災害時・物流・点検などの目視外が重要となる所で活躍します。<br>
<br>
⑴ドローンのフライトプランの管理<br>
・飛行計画、飛行ログの管理<br>
・UAV飛行情報（位置、高度、速度、角度・バッテリー残量等）のリアルタイム管理<br>
<br>
⑵飛行エリアの管理<br>
・ジオフェンス：重要施設等飛行禁止区域、<br>
  ユーザーにより設定されたエリア内への進入禁止<br>
・ジオケージ（ジオフィルター）：指定エリア内のみでの運行<br>
<br>
⑶複数のUAV間<br>
・障害物間の衝突防止、緊急時の停止<br>
・自動帰還</p>
		</div>
	</div>

	<div class="uav_right">
	<ul class="imgslider-wrap">
		<li class="img_slider">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc1000.png">
</li>
<li class="img_slider bg_white">
<img src="<?php echo get_template_directory_uri(); ?>/assets/img/pc2000.png">
<p class="title">ドローンUAVのアプリ</p>
<p class="txt">Terra UTM app をお使いいただくことで、<br>
現場での安全な飛行を行っていただくことができます。</p>
</li>
</ul>
</div>
	</section>

<section class="s_price">
	<div class="container">
		<div class="inner">
			<p class="text_title">アカウント 月額/ 5,000円（基本保険料 込）</p>
			<p class="text_desc">飛行回数2回まで、<span class="bold">無料</span>でお試しいただけます。</p>
			<p class="text_desc margin"><a href="#">Terra Mapperクラウド版</a><span class="bold">ご利用のお客様は、Terra UTMを無料でご利用いただくことができます。</span></p>
			<p class="btn"><a href="<?php echo home_url(); ?>/account/">まずは無料ではじめる</a></p>
			<p class="text_desc2">※有料でのご使用に限り、保険適用となります。あらかじめご了承ください。</p>
		</div>
	</div>
</section>

<section class="s_utminfo">
	<div class="container">
	<ul id="slider_wrap">
	 <li class="utm_slider cf">
	 	<div class="slider_txt">
    <p class="txt_title">Terra UTM 詳細機能</p>
		<p class="txt_title2">①飛行情報の設定</p>
		<p class="txt_desc">様々なミッションに合わせて、飛行経路の設定が可能です。<br>
用途に応じて、自動運行や目視外での飛行、<br>
複数機体の同時制御を支援します。</p>
	 	</div>
		<div class="slider_img">
			<img class="wow slideInUp img01" data-wow-duration="3s" data-wow-delay="0s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm01.png"></img>
		</div>
	 </li>
	 <li class="utm_slider cf">
		 <div class="slider_txt">
		<p class="txt_title">Terra UTM 詳細機能</p>
	 <p class="txt_title2">②飛行直前の機体情報・環境情報の確認</p>
	 <p class="txt_desc">人口密集地域や天候情報などを、現場で確認することができます。<br>
適切なコンディションの元で安全な飛行計画を設計いただけます。</p>
	 </div>
	 <div class="slider_img">
		 <img class="wow slideInUp img02" data-wow-duration="3s" data-wow-delay="4s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm02.png"></img>
	 </div>
	 </li>
	 <li class="utm_slider cf">
		 <div class="slider_txt">
		<p class="txt_title">Terra UTM 詳細機能</p>
	 <p class="txt_title2">③必要な飛行申請フォームの自動生成定</p>
	 <p class="txt_desc">飛行情報や、飛行履歴などから必要な申請フォームを<br>
自動生成することができます。(準備中)</p>
	 </div>
	 <div class="slider_img">
		 <img class="wow slideInUp img03" data-wow-duration="3s" data-wow-delay="8s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm03.png"></img>
	 </div>
	 </li>
	 <li class="utm_slider cf">
		 <div class="slider_txt">
		<p class="txt_title">Terra UTM 詳細機能</p>
	 <p class="txt_title2">④遠隔制御</p>
	 <p class="txt_desc">現場に出ることなくドローンを事務所から遠隔制御することができます。<br>
ドローンで取得した映像を瞬時に遠隔で確認することができます。</p>
	 </div>
	 <div class="slider_img">
		 <img class="wow slideInUp img04" data-wow-duration="3s" data-wow-delay="12s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm04.png"></img>
	 </div>
	 </li>
	 <li class="utm_slider cf">
		 <div class="slider_txt">
		<p class="txt_title">Terra UTM 詳細機能</p>
	 <p class="txt_title2">⑤自動制御機能</p>
	 <p class="txt_desc">人口密集地域や、立入禁止区域など飛行規制エリアに機体が侵入した場合、<br>
自動的に飛行前に指定した地点へ帰還します。<br>
複数の機体の衝突も自動で防ぎます。 </p>
	 </div>
	 <div class="slider_img">
		 <img class="wow slideInUp img05" data-wow-duration="3s" data-wow-delay="16s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm05.png"></img>
	 </div>
	 </li>
	 <li class="utm_slider cf">
		 <div class="slider_txt">
		<p class="txt_title">Terra UTM 詳細機能</p>
	 <p class="txt_title2">⑥Terra Mapperとの連携</p>
	 <p class="txt_desc">Terra UTM で取得したデータは、<br>
自動的にTerra Mapper クラウド版 にデータ転送できます。<br>
転送された写真を元に三次元データを自動生成します。<br>
<a href="https://mapper.terra-drone.net/product/cloud/" class="btn">Terra Mapper クラウド版とは＞</a>
</p>
	 </div>
	 <div class="slider_img">
		 <img class="wow slideInUp img06" data-wow-duration="3s" data-wow-delay="20s" src="<?php echo get_template_directory_uri(); ?>/assets/img/utm06.png"></img>
	 </div>
	 </li>
	</ul>
	</div>
</section>

<section class="s_insurance">
<div class="container">
<div class="table_wrap cf">
	<div class="table01">
		<div class="utm_logo">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/insurance_terrautm.png"></img>
		</div>
		<div class="img_tokio">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/insurance_tokio.png"></img>
		</div>
		<p class="txt">Terra UTMを購入した際に、<br>
東京海上日動が提供する、対人・対物賠償責任保険が自動付帯。<br>
UTMアプリ利用中に万が一起きてしまった事故に対して、<br>
最大1億円の損害保険金をお支払いします。</p>
	</div>
		<div class="table02">
			<div class="table_up cf">
				<img style="margin-top: 4px;" src="<?php echo get_template_directory_uri(); ?>/assets/img/insurance_icon01.png">
        <p class="txt">アプリ利用と同時に<br>
自動で保険加入</p>
			</div>
			<div class="table_down">
				<p class="txt">月額5,000円<br>
（アプリ利用料）</p>
			</div>
		</div>
			<div class="table02">
				<div class="table_up cf">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/insurance_icon02.png">
	        <p class="txt" style="left: 115px;">安心の補償額</p>
				</div>
				<div class="table_down">
					<p class="txt">もしもの事故に<br>
最大1億円補償</p>
				</div>
			</div>
</div>
<div class="table_wrap2 cf">
	<div class="table03">
		<p class="title">賠償責任補償の事故例</p>
		<p class="desc">＜身体障害賠償＞<br>
ドローンが墜落し人に衝突。ケガを負わせてしまった。<br>
＜器物損壊賠償＞<br>
ドローンが不具合を起こし、他人の家の窓ガラスに墜落し、割ってしまった。</p>
		<p class="desc02">保険が適用される具体例 ：<br>
お客様が所有・使用または管理するドローンにより、万が一他人の生命・身体に影響を及ぼした場合や、
他人の所有物を破損・減失・汚損した場合に、法律上の損害賠償責任を担当する事によって被る損害に
対して、最大1億円の保険金をお支払い致します。</p>
	</div>
	<div class="table04">
		<p class="title">お支払の対象となる損害</p>
		<p class="desc">損害賠償金（1名／1事故)</p>
	</div>
</div>
</div>

</section>

<section class="s_case">
<div class="container">
<p class="title"><span class="border">Terra UTMユーザー事例</span></p>
<div class="case_wrap cf">
	<div class="case_item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/case01.png"></img>
<p class="case_text">安全な現場作業を支援</p>
	</div>
	<div class="case_item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/case02.png"></img>
<p class="case_text">工場の高所・危険個所<br>
道路・橋などのインフラの点検</p></div>
	<div class="case_item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/case03.png"></img>
<p class="case_text">農場・漁場での作業を効率化</p>
	</div>
	<div class="case_item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/case04.png"></img>
<p class="case_text">物流、配達が難しい地域への<br>
配達を効率化</p>
	</div>
	<div class="case_item">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/case05.png"></img>
<p class="case_text">警備のサービスを自動、高度に</p>
	</div>
</div>
</div>
</section>

</main><!-- /g_main -->

<?php get_footer(); ?>
